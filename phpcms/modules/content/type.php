 
<?php
defined('IN_PHPCMS') or exit('No permission resources.');
//模型缓存路径
define('CACHE_MODEL_PATH',CACHE_PATH.'caches_model'.DIRECTORY_SEPARATOR.'caches_data'.DIRECTORY_SEPARATOR);
pc_base::load_app_func('util','content');
class type {
private $db;
function __construct() {
  $this->db = pc_base::load_model('content_model');
}
/**
  * 按照模型搜索
  */
public function init() {
  if(!isset($_GET['catid'])) showmessage(L('missing_part_parameters'));
  $catid = intval($_GET['catid']);
  $siteids = getcache('category_content','commons');
  $siteid = $siteids[$catid];
  $this->categorys = getcache('category_content_'.$siteid,'commons');
  if(!isset($this->categorys[$catid])) showmessage(L('missing_part_parameters'));
  if(isset($_GET['info']['catid']) && $_GET['info']['catid']) {
   $catid = intval($_GET['info']['catid']);
  } else {
   $_GET['info']['catid'] = 0;
  }
  if(isset($_GET['typeid']) && trim($_GET['typeid']) != '') {
   $typeid = intval($_GET['typeid']);
  } else {
   showmessage(L('illegal_operation'));
  }
  $TYPE = getcache('type_content','commons');
  $modelid = $this->categorys[$catid]['modelid'];
  $modelid = intval($modelid);
  if(!$modelid) showmessage(L('illegal_parameters'));
  $CATEGORYS = $this->categorys;
  $siteid = $this->categorys[$catid]['siteid'];
  $siteurl = siteurl($siteid);
  $this->db->set_model($modelid);
  $page = $_GET['page'];
  $datas = $infos = array();
  $infos = $this->db->listinfo("`typeid` = '$typeid'",'id DESC',$page,20);//读取整个模型下同类别文章
  //$infos = $this->db->listinfo("`typeid` = '$typeid' AND catid = '$catid'",'id DESC',$page,20);//仅仅读取当前栏目下的同类别文章,如果要启用此模式,请去掉上一行代码并将本行开头的// 两斜杠去掉.
  $total = $this->db->number;
  if($total>0) {
   $pages = $this->db->pages;
   foreach($infos as $_v) {
    if(strpos($_v['url'],'://')===false) $_v['url'] = $siteurl.$_v['url'];
    $datas[] = $_v;
   }
  }
  $SEO = seo($siteid, $catid, $TYPE[$typeid]['name'],$TYPE[$typeid]['description'],$TYPE[$typeid]['name'].'类别');
  include template('content','type');
}
}
?>