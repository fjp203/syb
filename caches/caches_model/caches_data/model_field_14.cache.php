<?php
return array (
  'keshi' => 
  array (
    'fieldid' => '110',
    'modelid' => '14',
    'siteid' => '0',
    'field' => 'keshi',
    'name' => '科室',
    'tips' => '',
    'css' => '',
    'minlength' => '1',
    'maxlength' => '0',
    'pattern' => '',
    'errortips' => '',
    'formtype' => 'box',
    'setting' => 'array (
  \'options\' => \'人力资源科|人力资源科
综合科|综合科
食堂科|食堂科\',
  \'boxtype\' => \'radio\',
  \'fieldtype\' => \'varchar\',
  \'minnumber\' => \'1\',
  \'width\' => \'100\',
  \'size\' => \'1\',
  \'defaultvalue\' => \'\',
  \'outputtype\' => \'1\',
)',
    'formattribute' => '',
    'unsetgroupids' => '',
    'unsetroleids' => '',
    'iscore' => '0',
    'issystem' => '0',
    'isunique' => '0',
    'isbase' => '1',
    'issearch' => '0',
    'isadd' => '1',
    'isfulltext' => '0',
    'isposition' => '0',
    'listorder' => '0',
    'disabled' => '0',
    'isomnipotent' => '0',
    'options' => '人力资源科|人力资源科
综合科|综合科
食堂科|食堂科',
    'boxtype' => 'radio',
    'fieldtype' => 'varchar',
    'minnumber' => '1',
    'width' => '100',
    'size' => '1',
    'defaultvalue' => '',
    'outputtype' => '1',
  ),
);
?>