<?php
return array (
  'keshi' => 
  array (
    'fieldid' => '108',
    'modelid' => '12',
    'siteid' => '0',
    'field' => 'keshi',
    'name' => '科室',
    'tips' => '',
    'css' => '',
    'minlength' => '1',
    'maxlength' => '0',
    'pattern' => '',
    'errortips' => '',
    'formtype' => 'box',
    'setting' => 'array (
  \'options\' => \'技术科|技术科
工艺科|工艺科
数据信息科|数据信息科\',
  \'boxtype\' => \'radio\',
  \'fieldtype\' => \'varchar\',
  \'minnumber\' => \'1\',
  \'width\' => \'100\',
  \'size\' => \'1\',
  \'defaultvalue\' => \'\',
  \'outputtype\' => \'1\',
)',
    'formattribute' => '',
    'unsetgroupids' => '',
    'unsetroleids' => '',
    'iscore' => '0',
    'issystem' => '0',
    'isunique' => '0',
    'isbase' => '1',
    'issearch' => '0',
    'isadd' => '1',
    'isfulltext' => '0',
    'isposition' => '0',
    'listorder' => '0',
    'disabled' => '0',
    'isomnipotent' => '0',
    'options' => '技术科|技术科
工艺科|工艺科
数据信息科|数据信息科',
    'boxtype' => 'radio',
    'fieldtype' => 'varchar',
    'minnumber' => '1',
    'width' => '100',
    'size' => '1',
    'defaultvalue' => '',
    'outputtype' => '1',
  ),
);
?>