<?php
return array (
  'title' => '部门科室',
  'style' => '2',
  'siteid' => '0',
  'data' => 
  array (
    3361 => 
    array (
      'linkageid' => '3361',
      'name' => '生产制造部',
      'style' => '0',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3361,3365,3389,3366,3368,3369,3370,3371,3372,3373,3374',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => 'array (
  \'level\' => \'0\',
)',
      'siteid' => '0',
    ),
    3363 => 
    array (
      'linkageid' => '3363',
      'name' => '技术部',
      'style' => '',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3363,3375,3376,3377',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3364 => 
    array (
      'linkageid' => '3364',
      'name' => '质量管理部',
      'style' => '',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3364,3378,3379',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3365 => 
    array (
      'linkageid' => '3365',
      'name' => '生产科',
      'style' => '0',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3365',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => 'array (
  \'level\' => \'0\',
)',
      'siteid' => '0',
    ),
    3367 => 
    array (
      'linkageid' => '3367',
      'name' => '装配二车间',
      'style' => '0',
      'parentid' => '3367',
      'child' => '0',
      'arrchildid' => '3367',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => 'array (
  \'level\' => \'0\',
)',
      'siteid' => '0',
    ),
    3375 => 
    array (
      'linkageid' => '3375',
      'name' => '技术科',
      'style' => '',
      'parentid' => '3363',
      'child' => '0',
      'arrchildid' => '3375',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3376 => 
    array (
      'linkageid' => '3376',
      'name' => '工艺科',
      'style' => '',
      'parentid' => '3363',
      'child' => '0',
      'arrchildid' => '3376',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3377 => 
    array (
      'linkageid' => '3377',
      'name' => '数据信息科',
      'style' => '',
      'parentid' => '3363',
      'child' => '0',
      'arrchildid' => '3377',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3378 => 
    array (
      'linkageid' => '3378',
      'name' => '质量科',
      'style' => '',
      'parentid' => '3364',
      'child' => '0',
      'arrchildid' => '3378',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3379 => 
    array (
      'linkageid' => '3379',
      'name' => '检查科',
      'style' => '',
      'parentid' => '3364',
      'child' => '0',
      'arrchildid' => '3379',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3380 => 
    array (
      'linkageid' => '3380',
      'name' => '综合管理部',
      'style' => '',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3380,3381,3382',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3381 => 
    array (
      'linkageid' => '3381',
      'name' => '综合科',
      'style' => '',
      'parentid' => '3380',
      'child' => '0',
      'arrchildid' => '3381',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3382 => 
    array (
      'linkageid' => '3382',
      'name' => '食堂科',
      'style' => '',
      'parentid' => '3380',
      'child' => '0',
      'arrchildid' => '3382',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3383 => 
    array (
      'linkageid' => '3383',
      'name' => '计划财务部',
      'style' => '',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3383,3385,3386',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3384 => 
    array (
      'linkageid' => '3384',
      'name' => '市场部',
      'style' => '',
      'parentid' => '0',
      'child' => '1',
      'arrchildid' => '3384,3387,3388',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3385 => 
    array (
      'linkageid' => '3385',
      'name' => '财务科',
      'style' => '',
      'parentid' => '3383',
      'child' => '0',
      'arrchildid' => '3385',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3386 => 
    array (
      'linkageid' => '3386',
      'name' => '运营科',
      'style' => '',
      'parentid' => '3383',
      'child' => '0',
      'arrchildid' => '3386',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3387 => 
    array (
      'linkageid' => '3387',
      'name' => '市场科',
      'style' => '',
      'parentid' => '3384',
      'child' => '0',
      'arrchildid' => '3387',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3388 => 
    array (
      'linkageid' => '3388',
      'name' => '采购科',
      'style' => '',
      'parentid' => '3384',
      'child' => '0',
      'arrchildid' => '3388',
      'keyid' => '3360',
      'listorder' => '0',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3389 => 
    array (
      'linkageid' => '3389',
      'name' => '装配一车间',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3389',
      'keyid' => '3360',
      'listorder' => '1',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3366 => 
    array (
      'linkageid' => '3366',
      'name' => '装配二车间',
      'style' => '0',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3366',
      'keyid' => '3360',
      'listorder' => '2',
      'description' => '',
      'setting' => 'array (
  \'level\' => \'0\',
)',
      'siteid' => '0',
    ),
    3368 => 
    array (
      'linkageid' => '3368',
      'name' => '试制一车间',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3368',
      'keyid' => '3360',
      'listorder' => '3',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3369 => 
    array (
      'linkageid' => '3369',
      'name' => '试制二车间',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3369',
      'keyid' => '3360',
      'listorder' => '4',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3370 => 
    array (
      'linkageid' => '3370',
      'name' => '内饰车间',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3370',
      'keyid' => '3360',
      'listorder' => '5',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3371 => 
    array (
      'linkageid' => '3371',
      'name' => '调试车间',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3371',
      'keyid' => '3360',
      'listorder' => '6',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3372 => 
    array (
      'linkageid' => '3372',
      'name' => '物料科',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3372',
      'keyid' => '3360',
      'listorder' => '7',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
    3373 => 
    array (
      'linkageid' => '3373',
      'name' => '安技科',
      'style' => '0',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3373',
      'keyid' => '3360',
      'listorder' => '8',
      'description' => '',
      'setting' => 'array (
  \'level\' => \'0\',
)',
      'siteid' => '0',
    ),
    3374 => 
    array (
      'linkageid' => '3374',
      'name' => '设备科',
      'style' => '',
      'parentid' => '3361',
      'child' => '0',
      'arrchildid' => '3374',
      'keyid' => '3360',
      'listorder' => '9',
      'description' => '',
      'setting' => '',
      'siteid' => '0',
    ),
  ),
);
?>